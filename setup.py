from os.path import join, dirname
from setuptools import setup


version = __import__('doorkeeper').__version__

LONG_DESCRIPTION = """
Anti bad bot - bans for malicious url requests
"""


def long_description():
    """Return long description from README.rst if it's present
    because it doesn't get installed."""
    try:
        return open(join(dirname(__file__), 'README.rst')).read()
    except IOError:
        return LONG_DESCRIPTION

setup(
    name='django-doorkeeper',
    version='1.0',
    description='Anti bad bot - bans for malicious url requests',
    author='Eugene Gavrish',
    author_email='eug033@gmail.com',
    url='https://bitbucket.org/EGh5/django-doorkeeper',
    license='MIT',
    py_modules=('django-doorkeeper',),
    zip_safe=True,
    packages=['doorkeeper',],
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
)