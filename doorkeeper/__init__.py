"""
Django-doorkeeper helps to deal with malicious url requests
"""
version = (1,6,0)
__version__ = '.'.join(map(str, version))
