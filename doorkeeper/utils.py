# -*- coding: utf-8 -*-
# Creation date: 18.03.12

import memcache

class Singleton(type):
    """
    Metaclass  for singleton conversion for memcache.Client
    """
    def __init__(cls, name, bases, dict):
         super(Singleton, cls).__init__(name, bases, dict)
         cls.instance = None
    def __call__(cls,*args,**kw):
         if cls.instance is None:
             cls.instance = super(Singleton, cls).__call__(*args, **kw)
         return cls.instance

class MemClient(memcache.Client):
     __metaclass__ = Singleton

