# -*- coding: utf-8 -*-
# Creation date: 10.03.12

from doorkeeper.models import BotRecord
from django.contrib import admin


class BotRecordAdmin(admin.ModelAdmin):
    list_display = ('bot_adr','bot_name','jail_count','last_date','penalty_count')

admin.site.register(BotRecord,BotRecordAdmin)
