# -*- coding: utf-8 -*-
from django.db import models

class BotRecord(models.Model):
    bot_name = models.CharField("имя бота",max_length=250)
    bot_adr = models.CharField("адрес",max_length=250)
    bot_hash = models.CharField("хэш",max_length=250)                        # primary_key=True ?
    jail_count = models.IntegerField("запретов",default=0)
    penalty_count = models.IntegerField("штрафы",default=0)
    last_date=models.DateTimeField("обновлено",null=True,auto_now=True)
    detention_upto=models.DateTimeField("блок до",null=True)

    def __unicode__(self):
        return "%s" % self.bot_hash

