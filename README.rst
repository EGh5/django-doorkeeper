Django Doorkeeper
=================

Features
--------
This application bannes bad bots making requests for exactly non-existent urls.
Middleware-level is using.

Since version 1.5 daemon *memcached* can be used. In this case middleware
uses memory only operations. All overhead in this case is: md5 calculation 
for string  50-80 chars and  *memcahed* inquiry for key existance!


Installation
------------

Use standard python way::

  python setup.py install
 
Package doorkeeper will be created in your site Python or virtenv.

Another way - copy folder doorkeeper as application in your site folder.

You should add patterns of this urls as LAST tuple elements in your urls.py::

  from doorkeeper.views import inspect
  ...    
  urlpatterns = patterns('',
    ....
    ....
    url(r'php',inspect),
    url(r'translators\.html',inspect),
    url(r'dba',inspect),
    )

You can add this constants in your settings.py::

  DOORKEEPER_PATIENCE=3  # number of bad attempts
  DOORKEEPER_TERM=1200   # penalty time (sec.)
  
If memcached is available, strictly recommended to use it adding 
its address (or list of addresses).::

  DOORKEEPER_MEMCACHE=['127.0.0.1:11211']

Also you can avoid smart slash adding, which lead to Http404 
duplicating for urls, example: /dddf --> /ddddf/ . 
To do this just set Django variable in settings.py::

  APPEND_SLASH = False

(But don't forget to use /admin/ (with last slash) is this case).

  
Add to INSTALLED_APPS,::

  'doorkeeper', 

and to MIDDLEWARE_CLASSES (as high as possible)::

  'doorkeeper.middleware.DoorkeeperMiddleware',
 

Monitoring
------------

You can watch your catch in django-admin.
 
NOTE: this application is not intendent for high-loaded django-site. 
Please consider os-level utilities. fail2ban should be mentioned among them.

Check actual revision in `Bitbucket repository`_

Good luck in hunting!

Eugene

.. _Bitbucket repository: https://bitbucket.org/EGh5/django-doorkeeper
